package com.Nowak;

import java.util.List;

public class ShipsBuilder {

    private int size4 = 2;
    private int size3 = 3;
    private int size2 = 4;
    private int size1 = 5;

    public ShipsBuilder() {
    }

    public ShipsBuilder(int size1, int size2, int size3, int size4) {
        this.size1 = size1;
        this.size2 = size2;
        this.size3 = size3;
        this.size4 = size4;
    }

    public  int summedNumberOfShips(){
        return size1+size2+size3+size4;
    }

    private int numberOfShips(int lenght) {
        if (lenght == 1) {
            return size1;
        } else if (lenght == 2) {
            return size2;
        } else if (lenght == 3) {
            return size3;
        } else {
            return size4;
        }
    }

    private void sizeMinusOne (int lenght){
        if (lenght == 1) {
            this.size1--;
        } else if (lenght == 2) {
            this.size2--;
        } else if (lenght == 3) {
            this.size3--;
        } else {
            this.size4--;
        }
    }


    public void build(List<Character> board, int square, int lenght, int orientation, Character shipSign) {//statek dowolnej długości i orientacji: 1 - poziomo, 12 - pionowo

        if (numberOfShips(lenght) > 0) {

            for (int i = 0; i < lenght; i++)
                board.set((square + i * orientation), shipSign);

            sizeMinusOne(lenght);
        }
    }
}
