package com.Nowak;

import java.util.List;

public class Print {

    public static void print(List<Character> board) {
        int i = 0;
        int j = 0;

        System.out.println("\u2001 A\u2001B\u2001C\u2001D\u2001E\u2001F\u2001G\u2001H\u2001I\u2001J");

        while (i < 10) {
            System.out.print(' ' + "\u2001");
            while (j < (10 + i * 10)) {
                System.out.print(board.get(j) + "\u2001");
                j++;
            }
            System.out.print(i+1);
            System.out.println();
            i++;
        }
        System.out.println("\u2001 A\u2001B\u2001C\u2001D\u2001E\u2001F\u2001G\u2001H\u2001I\u2001J");
    }
}
