package com.Nowak;

import java.util.List;

public class Conditions {

    public static boolean conditionH1(int square, int lenght) {
        for (int row = 0; row < 11; row++)
            if (square > (12 + (12 * row)) && square < ((24 - lenght) + 12 * row))
                return true;

        return false;
    }

    public static boolean conditionV1(int square, int lenght) {


        for (int row = 0; row < 11; row++)
            if (square > (12 + 12 * row) && square < (23 + 12 * row)) {
                if (square > 12 && square < (143 - 12 * lenght))
                    return true;
            }

        return false;
    }

    public static boolean conditionH2(List<Character> board, int square, int lenght, Character shipSign) {
        int counter = 0;

        for (int currentLenght = lenght; currentLenght >= -1; currentLenght--) {
            if (!board.get((square + currentLenght) - 12).equals(shipSign)
                    && !board.get(square + currentLenght).equals(shipSign)
                    && !board.get((square + currentLenght) + 12).equals(shipSign)) {
                counter++;
            }
        }

        if (counter == (lenght + 2))
            return true;

        return false;
    }

    public static boolean conditionV2(List<Character> board, int square, int lenght, Character shipSign) {
        int counter = 0;

        for (int currentLenght = lenght; currentLenght >= -1; currentLenght--) {
            if (!board.get((square + (12 * currentLenght)) - 1).equals(shipSign)
                    && !board.get(square + (12 * currentLenght)).equals(shipSign)
                    && !board.get((square + (12 * currentLenght)) + 1).equals(shipSign)) {
                counter++;
            }
        }

        if (counter == (lenght + 2))
            return true;

        return false;
    }


    public static boolean allConditions (List<Character> board, int square, int lenght, Character shipSign, int orientation) {

        if (orientation == 1) {
            if (conditionH1(square, lenght) && conditionH2(board, square, lenght, shipSign)){
                return true;
            }
        } else if (orientation == 12) {
            if (conditionV1(square, lenght) && conditionV2(board, square, lenght, shipSign)){
                return true;
            }
        }
        return false;
    }

}


// warunek  1
// statek może być budowany na określonych polach:
// 1. w kwadracie 10x10
// 2. (tylko dla H)tak aby nie był podzielony prechodząc na następną linię

//warunek2
// statek nie może nachodzić ani stykać się z innym statkiem:
// 1. pole na którym stoimy nie jest już zajęte
// 2. pola do okoła przyszłego statku nie są już zajęte




// 0   1   2   3   4   5   6   7   8   9  10  11
//12  13  14  15  16  17  18  19  20  21  22  23
//24  25  26  27  28  29  30  31  32  33  34  35
//36
//48  49  50  51  52  53  54  55  56  57  58  59
//60
//72
//84  85  86  87  88  89  90  91  92  93  94  95
//96
//108 109 110 111 112 113 114 115 116 117 118 119
//120
//132 133 134 135 136 137 138 139 140 141 142 143
