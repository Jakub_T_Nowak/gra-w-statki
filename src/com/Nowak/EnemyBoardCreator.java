package com.Nowak;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EnemyBoardCreator {

    Random rand = new Random();

    public List<Character> bigBoard = new ArrayList<>();
    public List<Character> board = new ArrayList<>();
    ShipsBuilder ships = new ShipsBuilder();

    Character shipChar = 's';
    Character waterChar = '.';




    private List<Character> emptyBoard() {
        for (int i = 0; i < 144; i++)
            bigBoard.add(waterChar);

        return bigBoard;
    }

    private List<Character> resize(List bigBoard) {

        for (int square = 13; square < 133; square++) {
            for (int row = 0; row < 11; row++) {
                if (square > (12 + 12 * row) && square < (23 + 12 * row)) {
                    this.board.add((Character) bigBoard.get(square));
                }
            }
        }
        return this.board;
    }

    public List<Character> creator() {

        emptyBoard();

        for (int square = 13; square < 133; square++) {

            int randomOrientation = rand.nextInt(2) * 11 + 1;
            int randomLenght = rand.nextInt(4) + 1;

            if (Conditions.allConditions(bigBoard, square, randomLenght, shipChar, randomOrientation)) {
                ships.build(bigBoard, square, randomLenght, randomOrientation, shipChar);
            }
        }

        if (ships.summedNumberOfShips() != 0)
            creator();

        return resize(bigBoard);
    }
}
